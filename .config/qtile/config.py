# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import subprocess

from libqtile.config import Key, Screen, Group, Drag, Click
from libqtile.lazy import lazy
from libqtile import layout, bar, widget, hook
from typing import List  # noqa: F401

# add autostart
@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~')
    subprocess.Popen([home + '/.config/qtile/autostart.sh'])

# Define some variables
mod = "mod4"
myTerm = "alacritty"
myConfig = "$HOME/.config/qtile/config.py"

keys = [
    # Window movements (mod + [key] or mod + shift + [key])
    Key(
        [mod], "k",
        lazy.layout.down(),
        "move focus up"
        ),
    Key(
        [mod], "j", 
        lazy.layout.up(),
        "move focus down"
        ),

    Key(
        [mod, "shift"], "k",
        lazy.layout.shuffle_down(),
        "move windows down"),
    Key(
        [mod, "shift"], "j",
        lazy.layout.shuffle_up(),
        "move windows up"
        ),

    Key(
        [mod], "h",
        lazy.layout.grow(),
        lazy.layout.increase_nmaster(),
        desc='Expand window (MonadTall), increase number in master pane (Tile)'
        ),
    Key(
        [mod], "l",
        lazy.layout.shrink(),
        lazy.layout.decrease_nmaster(),
        desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
        ),
    Key(
        [mod], "n",
        lazy.layout.normalize(),
        desc='normalize window size ratios'
        ),
    Key(
        [mod], "m",
        lazy.layout.maximize(),
        desc='toggle window between minimum and maximum sizes'
        ),
    Key(
        [mod, "shift"], "f",
        lazy.window.toggle_floating(),
        desc='toggle floating'
        ),

    Key(
        [mod], "space",
        lazy.layout.next(),
        "switch through windows"
        ),

    Key(
        [mod], "Tab",
        lazy.next_layout(),
        "toggle layouts"
        ),

    Key(
        [mod], "q",
        lazy.window.kill(),
        "kill a window"
        ),

    Key(
        [mod, "shift"], "r",
        lazy.restart(),
        "restart qtile"
        ),

    # Sound Control
    Key(
        [], "XF86AudioMute",
        lazy.spawn("amixer -q set Master toggle"),
        ),
    Key(
        [], "XF86AudioLowerVolume",
        lazy.spawn("amixer -c 0 sset Master 4- unmute"),
        ),
    Key(
        [], "XF86AudioRaiseVolume",
        lazy.spawn("amixer -c 0 sset Master 4+ unmute"),
        ),

    # Backlight Control
    Key(
        [],"XF86MonBrightnessUp",
        lazy.spawn("xbacklight -inc 5 "),
        ),
    Key(
        [],"XF86MonBrightnessDown",
        lazy.spawn("xbacklight -dec 5"),
        ),


    # Applications

    # Terminal(mod + mod1 + [key])
    Key(
        [mod], "Return",
        lazy.spawn(myTerm),
        "open my terminal emulator"
        ),

    Key(
        [mod, "mod1"], "e",
        lazy.spawn("emacs"),
        "launch emacs"
        ),
    Key(
        [mod, "mod1"], "f",
        lazy.spawn("pcmanfm"),
        "launch file explorer"
        ),
    Key(
        [mod, "mod1"], "b",
        lazy.spawn("firefox"),
        "launch firefox"
        ),
    Key(
        [mod, "mod1"], "r",
        lazy.spawn(myTerm+" -e tuir"),
        desc='tui reddit'
        ),

    #Dmenu Settings (mod + control + [key])
    Key(
        [mod], "d",
        lazy.spawn("dmenu_run"),
        "launch application starter"
        ),

]

##### DRAG FLOATING WINDOWS #####
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False


layout_theme = {"border_width": 1,
                "margin": 16,
                "border_focus": "D14E4E",
                "border_normal": "1D2330"
                }

layouts = [
    # layout.Max(),
    # layout.Stack(num_stacks=2),
    # Try more layouts by unleashing below layouts.
    # layout.Bsp(),
    # layout.Columns(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
    layout.MonadTall(**layout_theme),
    # layout.Max(**layout_theme),
    layout.Tile(shift_windows=True, **layout_theme),
    layout.Stack(num_stacks=2),
    layout.TreeTab(
         font = "Hack Nerd Font",
         fontsize = 10,
         sections = ["FIRST", "SECOND"],
         section_fontsize = 11,
         bg_color = "141414",
         active_bg = "90C435",
         active_fg = "000000",
         inactive_bg = "384323",
         inactive_fg = "a0a0a0",
         padding_y = 5,
         section_top = 10,
         panel_width = 320
         ),
     # layout.Floating(**layout_theme)

]

##### GROUPS #####
group_names = [("Bro-1", {'layout': 'monadtall'}),
               ("Dev-2", {'layout': 'monadtall'}),
               ("Sys-3", {'layout': 'monadtall'}),
               ("Doc-4", {'layout': 'monadtall'}),
               ("Irc-5", {'layout': 'monadtall'}),
               ("Mus-6", {'layout': 'monadtall'}),
               ("Vid-7", {'layout': 'monadtall'}),
               ("Rtv-8", {'layout': 'monadtall'})]

groups = [Group(name, **kwargs) for name, kwargs in group_names]
 
for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group	




# groups = [Group(i) for i in "12345678"]
# 
# for i in groups:
#     keys.extend([
#         # mod1 + letter of group = switch to group
#         Key([mod], i.name, lazy.group[i.name].toscreen()),
# 
#         # mod1 + shift + letter of group = switch to & move focused window to group
#         Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True)),
#         # Or, use below if you prefer not to switch to that group.
#         # # mod1 + shift + letter of group = move focused window to group
#         # Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
#     ])

colors = [["#2B303B", "#2B303B"], # panel background
          ["#6B6E74", "#6B6E74"], # background for current screen tab
          ["#EFF1F5", "#EFF1F5"], # font color for group names
          ["#D14E4E", "#D14E4E"], # border line color for current tab
          ["#F3A3A3", "#F3A3A3"]] # window name

widget_defaults = dict(
    font='Hack',
    fontsize=16,
    padding=5,
    foreground=colors[2],
    background = colors[0],
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayout(
                    ),
                widget.Sep(
                    linewidth = 0,
                    top = 15,
                    ),
                widget.GroupBox(
                    disable_drag="true",
                    fontsize = 14,
                    borderwidth = 3,
                    active = colors[2],
                    inactive = colors[2],
                    rounded = False,
                    highlight_color = colors[1],
                    highlight_method = "line",
                    this_current_screen_border = colors[3],
                    this_screen_border = colors [4],
                    other_current_screen_border = colors[0],
                    other_screen_border = colors[0],
                    ),
                widget.Prompt(
                    padding=10,
                    background = colors[1],
                    ),
                widget.Sep(
                    linewidth = 0,
                    padding = 40,
                    ),
                widget.WindowName(
                    foreground = colors[4],
                    padding = 0,
                    ),
                widget.Systray(),
                widget.Sep(
                    linewidth = 0,
                    padding = 20,
                    ),
                widget.TextBox(
                    text="| Vol",
                    background=colors[0],
                    ),
               widget.Volume(
                    background = colors[0],
                    padding = 5
                    ),
                widget.Net(
                    interface="wlo1",
                    format = '{down} ↓↑{up}',
                    background=colors[0],
                    ),
                widget.TextBox(
                    text = "| Bat:",
                    background=colors[0],
                    padding = 5,
                    ),
                widget.Battery(
                    format = "{char} {percent:2.0%} {hour:d}:{min:02d}",
                    background=colors[0],
                    ),
                widget.TextBox(
                    text = "|",
                    background=colors[0],
                    padding = 5,
                    ),
                widget.Clock(
                    background = colors[0],
                    format='%a %b-%d %H:%M'
                    ),
            ],
            30
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
