#!/bin/bash

export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
fcitx -d -r

xmodmap "$HOME/.xmodmap" &
picom &
flameshot > /dev/null&
nitrogen --restore &
electron-ssr > /dev/null &
