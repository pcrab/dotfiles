# Path to Oh My Fish install.
set -q XDG_DATA_HOME
  and set -gx OMF_PATH "$XDG_DATA_HOME/omf"
  or set -gx OMF_PATH "$HOME/.local/share/omf"

# Load Oh My Fish configuration.
source $OMF_PATH/init.fish

function fish_greeting
    paleofetch
#     echo ""
#     echo "                -@               " 
#     echo "               .##@              " 
#     echo "              .####@             " 
#     echo "              @#####@            " 
#     echo "            . *######@           " 
#     echo "           .##@o@#####@          " 
#     echo "          /############@         " 
#     echo "         /##############@        " 
#     echo "        @######@**%######@       " 
#     echo "       @######`     %#####o      " 
#     echo "      @######@       ######%     " 
#     echo "    -@#######h       ######@.`   " 
#     echo "   /#####h**``       `**%@####@  " 
#     echo "  @H@*`                    `*%#@ " 
#     echo " *`                            `*" 
end


set -x http_proxy "http://127.0.0.1:12333"
set -x https_proxy "http://127.0.0.1:12333"
set -x PATH "$PATH:/home/pcrab/.yarn/bin:/home/pcrab/.local/bin"
set -x EDITOR "nvim"


# alias

alias cal "cal -m"
alias vim nvim
alias lg lazygit
alias ra ranger
alias em "emacs -nw"
alias pandocn 'pandoc --pdf-engine=xelatex -V mainfont="Source Han Sans SC"'
alias diff meld
alias .. "cd .."
alias rm "rm -i"
alias mv "mv -i"
alias ls "exa"
alias ll "exa -lh"
alias l "exa -lah"
alias la l
alias gl "exa -lah --git-ignore"
alias top "htop"
alias emacs "env LC_CTYPE=zh_CN.UTF-8 emacs"
alias doas "doas --"
alias grep "grep --color=auto"

thefuck --alias | source
