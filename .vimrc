

"""""""""""""""""""
"Autoload vim-plug
"""""""""""""""""""

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif


" Compile function
map r :call CompileRunGcc()<CR>
func! CompileRunGcc()
  exec "w"
  if &filetype == 'c'
    exec "!g++ % -o %<"
    exec "!time ./%<"
  elseif &filetype == 'cpp'
    exec "!g++ % -o %<"
    exec "!time ./%<"
  elseif &filetype == 'java'
    exec "!javac %"
    exec "!time java %<"
  elseif &filetype == 'sh'
    exec "!time sh %"
  elseif &filetype == 'zsh'
    exec "!time zsh %"
  elseif &filetype == 'python'
    silent! exec "!clear"
    exec "!time python3 %"
  elseif &filetype == 'html'
    exec "!chromium % &"
  elseif &filetype == 'tex'
    silent! exec "!xelatex %"
  endif
endfunc


" ===
" === System
" ===
set nocompatible
filetype on
filetype indent on
filetype plugin on
filetype plugin indent on
set mouse=a
set encoding=utf-8

set clipboard=unnamed

" Prevent incorrect backgroung rendering
let &t_ut=''

" ===
" === Main code display
" ===
set number
set ruler
syntax enable
syntax on

" ===
" === Editor behavior
" ===
" Better tab
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4
set list
set listchars=tab:▸\ ,trail:▫
set scrolloff=5

" Prevent auto line split
set wrap
set tw=0

set indentexpr=
" Better backspace
set backspace=indent,eol,start

set foldmethod=indent
set foldlevel=99

let &t_SI = "\<Esc>]50;CursorShape=1\x7"
let &t_SR = "\<Esc>]50;CursorShape=2\x7"
let &t_EI = "\<Esc>]50;CursorShape=0\x7"

" ===
" === Window behaviors
" ===
set splitright
set splitbelow

" ===
" === Status/command bar
" ===
set laststatus=2
set autochdir
set showcmd
set formatoptions-=tc

" Show command autocomplete
set wildignore=log/**,node_modules/**,target/**,tmp/**,*.rbc
set wildmenu                                                 " show a navigable menu for tab completion
set wildmode=longest,list,full

" Searching options
set hlsearch
exec "nohlsearch"
set incsearch
set ignorecase
set smartcase

" ===
" === Basic Mappings
" ===
" ursor Movement
"===
"
" New cursor movement (the default arrow keys are used for resizing windows)
"     ^
"     k
" < h   l >
"     j
"     v

" I/K keys for 5 times u/e (faster navigation)
noremap K 5kzz
noremap J 5jzz
" J key: go to the start of the line
noremap H ^
" L key: go to the end of the line
noremap L $

" Faster in-line navigation
noremap W 5w
noremap B 5b

noremap Q :q<CR>
noremap W :w<CR>
noremap X :x<CR>

map s <nop>

" Set <LEADER> as <SPACE>
let mapleader=" "

" Set Search
noremap = nzz
noremap - Nzz
noremap <LEADER><CR> :nohlsearch<CR>

" Change Window Size
map <up> :res +5<CR>
map <down> :res -5<CR>
map <left> :vertical resize-5<CR>
map <right> :vertical resize+5<CR>

"Tab operations
map tt :tabe<CR>

map th :-tabnext<CR>
map tl :+tabnext<CR>

" Spill Check
  "map <LEADER>sc :set spell!<CR>
  "inoremap <C-x> <ESC>ea<C-x>s
  "noremap <C-x> ea<C-x>s

" Press space twice to jump to the next '
" ' and edit it
map <LEADER><LEADER> <Esc>/<++><CR>:nohlsearch<CR>c4i
map <LEADER>= <ESC>i<++><ESC>
inoremap <C-=> <ESC>a<++>

" remember location
if has("autocmd")
au BufReadPost * if line("`\"") > 1 && line("`\"") <= line("$") | exe "normal! g`\"" | endif
" for simplicity, "  au BufReadPost * exe "normal! g`\"", is Okay.
endif

call plug#begin('~/.vim/vimplugin')

Plug 'Raimondi/delimitMate'

"color theme
"
Plug 'dylanaraps/wal'
colorscheme wal
set background=light

" set termguicolors
" colorscheme snazzy
" let g:space_vim_transp_bg = 1

" ===
" vim-rainbow
" ===
Plug 'frazrepo/vim-rainbow'
let g:rainbow_active = 1


" ===
" gitgutter
" ===
Plug 'airblade/vim-gitgutter'



" "start menu
" Plug 'mhinz/vim-startify'


" "airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
let g:airline_theme='pencil'

Plug 'reedes/vim-colors-pencil'

" limelight
" Plug 'junegunn/limelight.vim'

" ===
" r-ide
" ===
Plug 'jalvesaq/Nvim-R', {'for' :['r'] }

" ===
" wechat-miniprogram-support
" ===
Plug 'chemzqm/wxapp.vim'

" ===
" fish shell script support
" ===
Plug 'Stautob/vim-fish'

" ===
" === ycm support
" ===
"Plug 'ycm-core/YouCompleteMe'
"let g:ycm_clangd_binary_path = "/usr/bin/clangd"
"
""configure ycm for better css completion
"let g:ycm_semantic_triggers = {
"    \   'css': [ 're!^\s{4}', 're!:\s+'],
"    \   'html': [ '</' ],
"    \ }


" html support
Plug 'mattn/emmet-vim'
Plug 'othree/html5.vim'

" css support
Plug 'ap/vim-css-color'
Plug 'hail2u/vim-css3-syntax'

" Ultisnips
Plug 'SirVer/ultisnips'
let g:UltiSnipsExpandTrigger = '<tab>'
let g:UltiSnipsJumpForwardTrigger = '<tab>'
let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'

Plug 'honza/vim-snippets'

" let g:UltiSnipsExpandTrigger="<c-e>"
" let g:UltiSnipsJumpForwardTrigger="<c-s>"
" let g:UltiSnipsJumpBackwardTrigger="<c-d>"

" LaTeX
Plug 'lervag/vimtex'
let g:tex_flavor='latex'
let g:vimtex_view_method='zathura'
let g:vimtex_quickfix_mode=0
Plug 'KeitaNakamura/tex-conceal.vim'
set conceallevel=1
hi Conceal ctermfg = White

" 注释
Plug 'scrooloose/nerdcommenter'

" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1
" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'
" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1
" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1
" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1
" Enable NERDCommenterToggle to check all selected lines is commented or not 
let g:NERDToggleCheckAllLines = 1


" ===
" === FZF
" ===
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
map <c-p> :FZF<CR>

" ===
" === vim-table-mode
" ===
Plug 'dhruvasagar/vim-table-mode', { 'on': 'TableModeToggle' }
map <LEADER>tm :TableModeToggle<CR>


" ===
" === calendar-vim
" ===
" Plug 'mattn/calendar-vim'
" noremap <LEADER>c :Calendar<CR>
" 
" function! ToggleCalendar()
"   execute ":Calendar"
"   if exists("g:calendar_open")
"     if g:calendar_open == 1
"       execute "q"
"       unlet g:calendar_open
"     else
"       g:calendar_open = 1
"     end
"   else
"     let g:calendar_open = 1
"   end
" endfunction


" File navigation

" ===
" === NERDTree
" ===
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
let NERDTreeMapOpenExpl = ""
let NERDTreeMapUpdir = ""
let NERDTreeMapUpdirKeepOpen = ""
let NERDTreeMapOpenSplit = ""
let NERDTreeOpenVSplit = ""
let NERDTreeMapActivateNode = "i"
let NERDTreeMapOpenInTab = "o"
let NERDTreeMapPreview = ""
let NERDTreeMapCloseDir = "n"
let NERDTreeMapChangeRoot = "y"

autocmd StdinReadPre * let s:std_in=1
" autocmd vimenter * NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

"NERDTree 窗口快捷键
map <C-tab> :NERDTreeToggle<CR>

Plug 'Xuyuanp/nerdtree-git-plugin'


" Other visual enhancement
" ===
" === vim-indent-guide
" ===
Plug 'Yggdroot/indentLine'
let g:indentLine_char = '|'
let g:indentLine_color_term = 238
let g:indentLine_color_gui = '#333333'
silent! unmap <LEADER>ig
autocmd WinEnter * silent! unmap <LEADER>ig


" Plug 'itchyny/vim-cursorword'
" Plug 'tmhedberg/SimpylFold'

" ===
" === vim dwm mode
" ===
Plug 'spolu/dwm.vim'

" ===
" === vim syntax for stylus
" ===
Plug 'wavded/vim-stylus'


call plug#end()
